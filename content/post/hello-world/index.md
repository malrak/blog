---
title: Hello World
description: Welcome to my blog!
slug: hello-world
date: 2024-09-14 00:00:00+0000
#image: cover.jpg
categories:
    - Linux Category
tags:
    - Peruse
    - KDE
    - Typst
# weight: 1       # You can add weight to some posts to override the default sorting (date descending)
---

## Introduction

Welcome to my blog, where I want to post about all things Linux, KDE, Security & Typst.

Some things I work on in my spare time are working on Peruse from KDE and my Cookbook Template in Typst.

## Peruse

[Peruse](https://apps.kde.org/peruse/) is the comic book reader from [KDE](https://kde.org). The killer feature of Peruse is that it supports [Advanced Comic Book Format (ACBF)](https://acbf.fandom.com/wiki/Advanced_Comic_Book_Format_Wiki) metadata. One feature ACBF supports is panel by panel reading, when the metadata allows it.

To my knowledge there only exist two (!) Comic Book Readers in the world that support this feature. The first is [ACBF Viewer](https://acbf.fandom.com/wiki/ACBF_Viewer), written in [Kivy](https://kivy.org) and Python 2 and largely unmaintained. The other is Peruse.

It is currently not that actively developed, but as it is being written using KDEs [Kirigami](https://develop.kde.org/frameworks/kirigami/) framework, it can be run on Android, where I would like to read comics. So I am on and off tinkering with the code to re-enable the Android Support for modern versions of Android. You can find my branch [here](https://invent.kde.org/graphics/peruse/-/merge_requests/34) should you be interested. It currently builds upon work from [Carl Schwan](https://invent.kde.org/graphics/peruse/-/merge_requests/44) who did the heavy lifting of porting Peruse from Qt5 to Qt6.

## Typst Cookbook

One technology that is really exciting is [Typst](https://typst.app/), a typesetting program comparable to LateX but much more modern. It is better in almost every way compared to LaTeX: It is faster, easier to write and better scriptable. The speed of Typst enables also features like instant preview of changes. I am currently looking into porting my LaTeX Cookbook Template over to Typst, which can be found [here](https://github.com/remggo/cookbook-typst/). It is already quite promising, however there is one topic that does not yet work as expected. I cannot figure out how to swap the page layout depending on even or odd page numbering - a feature that is essential for printing the recipes into a book. I hope I will figure this out someday and share the solution here.

<!-- > Photo by [Pawel Czerwinski](https://unsplash.com/@pawel_czerwinski) on [Unsplash](https://unsplash.com/) -->