---
title: Links
links:
  - title: KDE Invent
    description: KDE Invent is the git server of KDE
    website: https://invent.kde.org
    image: https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/KDE_logo.svg/128px-KDE_logo.svg.png
menu:
    main: 
        weight: 4
        params:
            icon: link

comments: false
---

These are some useful links that I can recommend to check out.
